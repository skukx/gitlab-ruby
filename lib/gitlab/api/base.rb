module Gitlab
  module Api
    class Base
      attr_reader :connection

      def initialize(connection)
        @connection = connection
      end

      protected

      def handle_action(action)
        response = action.call
        Gitlab::Response.new(response, action)
      end

      private

      def with_pagination(args)
        proc do |page = nil|
          args[:page] = page unless page.nil?
          yield
        end
      end
    end
  end
end

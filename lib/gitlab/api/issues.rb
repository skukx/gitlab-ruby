module Gitlab
  module Api
    ##
    # Issues api client
    # @see https://docs.gitlab.com/ee/api/issues.html
    #
    class Issues < Api::Base
      PATH = 'issues'

      ##
      # Retrieve all issues user has access to
      #
      # @see https://docs.gitlab.com/ee/api/issues.html#list-issues
      # @param args [Hash] Arguments to pass to gitlab
      # @return [Gitlab::Response]
      #
      def list(args = {})
        action = with_pagination(args) { connection.get(PATH, args) }
        handle_action(action)
      end

      ##
      # Retrieve all issues within a group
      #
      # @see https://docs.gitlab.com/ee/api/issues.html#list-group-issues
      # @param group [String] The group to get issues for
      # @param args [Hash] Arguments to pass to gitlab
      # @return [Gitlab::Response]
      #
      def list_group(group, args = {})
        action = with_pagination(args) { connection.get(group_path(group), args) }
        handle_action(action)
      end

      ##
      # Retrieve all issues within a project
      #
      # @see https://docs.gitlab.com/ee/api/issues.html#list-project-issues
      # @param project [String] The project to get issues for
      # @param args [Hash] Arguments to pass to gitlab
      # @return [Gitlab::Response]
      #
      def list_project(project, args = {})
        action = with_pagination(args) { connection.get(project_path(project), args) }
        handle_action(action)
      end

      private

      def group_path(group)
        "groups/#{group}/#{PATH}"
      end

      def project_path(project)
        "projects/#{project}/#{PATH}"
      end
    end
  end
end

module Gitlab
  class Response < Faraday::Response
    include Gitlab::Pagination

    attr_reader :gateway, :action

    def initialize(response, action = proc {})
      super()
      finish(response.env)
      @action = action
    end
  end
end

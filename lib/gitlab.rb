require 'active_support/hash_with_indifferent_access'
require 'active_support/inflector'
require 'faraday'
require 'faraday_middleware'

require 'gitlab/version'
require 'gitlab/pagination'
require 'gitlab/response'

require 'gitlab/api/base'
require 'gitlab/api/issues'
require 'gitlab/client'

module Gitlab
  API_URL = 'https://gitlab.com/api/v4/'.freeze
end

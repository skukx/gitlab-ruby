module Gitlab
  module Pagination
    def current_page_number
      headers['x-page'].to_i
    end

    def next_page_number
      headers['x-next-page'].to_i
    end

    def prev_page_number
      headers['x-prev-page'].to_i
    end

    def total_results
      headers['x-total'].to_i
    end

    def total_pages
      headers['x-total-pages'].to_i
    end

    def results_per_page
      headers['x-per-page'].to_i
    end

    def next_page
      return nil if next_page_number.zero?

      response = @action.call(next_page_number)
      self.class.new(response, @action)
    end
  end
end

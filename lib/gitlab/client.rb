module Gitlab
  class Client
    attr_reader :connection

    def self.api(name)
      converted = name.to_s.classify.pluralize
      klass = Gitlab::Api.const_get(converted)
      define_method(name) { klass.new(connection) }
    end

    ##
    # Endpoint Definitions
    #
    api :issues

    def initialize(private_token:, default_headers: {})
      @private_token = private_token
      @default_headers = default_headers.reverse_merge(
        'PRIVATE-TOKEN': private_token
      )

      @connection = Faraday.new(url: API_URL) do |conn|
        conn.headers = @default_headers
        conn.request :json
        conn.response :json
        conn.response :logger
        conn.adapter  Faraday.default_adapter
      end
    end
  end
end
